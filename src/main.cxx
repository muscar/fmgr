#include <iomanip>
#include <iostream>
#include <map>
#include <sstream>
#include <experimental/filesystem>

#include <gtkmm.h>

namespace fs = std::experimental::filesystem;

struct file_list_ctx
{
    fs::path current_path_ = fs::current_path();
    bool show_hidden_ = false;
};

class file_list_view : public Gtk::TreeView
{
    struct file_list_model : Gtk::TreeModelColumnRecord
    {
        Gtk::TreeModelColumn<std::string> file_name_col;
        Gtk::TreeModelColumn<std::string> file_size_col;

        file_list_model()
        {
            add(file_name_col);
            add(file_size_col);
        }
    };

public:
    using Model = file_list_model;

private:
    Model model_;
    Glib::RefPtr<Gtk::ListStore> store_;
    std::map<std::string, int> column_idx_map_;

public:
    file_list_view()
    {
        store_ = Gtk::ListStore::create(model_);
        set_model(store_);
    }

    void init_columns()
    {
        column_idx_map_["File"] = append_column("File", model_.file_name_col);
        column_idx_map_["Size"] = append_column("Size", model_.file_size_col);
    }

    template<typename RenderFun>
    void add_column_renderer(std::string name, RenderFun f)
    {
        if (auto it = column_idx_map_.find(name); it != std::end(column_idx_map_)) {
            if (auto col = get_column(it->second - 1); col) {
                auto renderer = static_cast<Gtk::CellRendererToggle*>(col->get_first_cell());
                col->set_cell_data_func(*renderer, f);
            }
        }
    }

    const Model &model() const
    {
        return model_;
    }

    Glib::RefPtr<Gtk::ListStore> store() const
    {
        return store_;
    }

    void add_entry(const fs::path &path)
    {
        auto row = *store_->append();
        std::stringstream ss;
        if (!fs::is_directory(path)) {
            ss << std::fixed << std::setprecision(2) << fs::file_size(path) / 1024.0 << " Kb";
        } else {
            ss << "-";
        }
        row[model_.file_name_col] = path.filename().string();
        row[model_.file_size_col] = ss.str();
    }

    void show_dir(file_list_ctx &ctx, std::vector<fs::path> paths)
    {
        store_->clear();
        if (ctx.current_path_ != "/") {
            add_entry("..");
        }
        for (auto &&p : paths) {
            add_entry(p);
        }
        if (auto it = store_->children().begin(); it) {
            set_cursor(Gtk::TreePath(it));
        }
    }
};

class file_pane
{
    Gtk::ScrolledWindow scrolled_window_;
    file_list_view list_view_;
    file_list_ctx ctx_;

public:
    Gtk::ScrolledWindow &build_ui()
    {
        list_view_.init_columns();
        list_view_.add_column_renderer("File", [&](auto renderer, const auto &it) {
            std::string name = (*it)[list_view_.model().file_name_col];
            if (fs::is_directory(ctx_.current_path_ / name)) {
                name = "<b>[" + name + "]</b>";
            }
            if (auto text_renderer = dynamic_cast<Gtk::CellRendererText*>(renderer); text_renderer) {
                text_renderer->property_markup() = name;
            }
        });

        scrolled_window_.set_policy(Gtk::POLICY_AUTOMATIC, Gtk::POLICY_AUTOMATIC);
        scrolled_window_.add(list_view_);

        return scrolled_window_;
    }

    void show_dir(std::vector<fs::path> paths)
    {
        list_view_.show_dir(ctx_, paths);
    }

    file_list_view &list_view()
    {
        return list_view_;
    }

    file_list_ctx &ctx()
    {
        return ctx_;
    }
};

class fmgr_app
{
    Glib::RefPtr<Gtk::Application> gtk_app_;
    Gtk::Window window_;
    Gtk::Box box_;
    file_pane left_pane_;
    file_pane right_pane_;

public:
    fmgr_app(Glib::RefPtr<Gtk::Application> app) : gtk_app_{app} { }

    void build_ui()
    {
        window_.set_default_size(500, 500);
        window_.set_border_width(10);

        window_.signal_key_press_event().connect([&](GdkEventKey* event) {
            if (event->type == GDK_KEY_PRESS && event->keyval == GDK_KEY_h && (event->state & GDK_CONTROL_MASK) == GDK_CONTROL_MASK) {
                left_pane_.ctx().show_hidden_ = !left_pane_.ctx().show_hidden_;
            }
            left_pane_.show_dir(list_dir(left_pane_.ctx().current_path_, left_pane_.ctx().show_hidden_));
            return true;
        });

        box_.pack_start(left_pane_.build_ui(), true, true, 1);
        box_.pack_end(right_pane_.build_ui(), true, true, 1);

        setup_pane(left_pane_);
        setup_pane(right_pane_);

        window_.add(box_);

        window_.show_all_children();

        left_pane_.show_dir(list_dir(left_pane_.ctx().current_path_, left_pane_.ctx().show_hidden_));
        right_pane_.show_dir(list_dir(right_pane_.ctx().current_path_, right_pane_.ctx().show_hidden_));
    }

    void setup_pane(file_pane &pane)
    {
        pane.list_view().signal_row_activated().connect([&](const auto &path, auto *column) {
            auto selection = pane.list_view().get_selection();
            if (auto it = selection->get_selected(); it) {
                std::string name = (*it)[pane.list_view().model().file_name_col];
                if (name == "..") {
                    pane.ctx().current_path_ = pane.ctx().current_path_.parent_path();
                } else if (fs::is_directory(pane.ctx().current_path_ / name)) {
                    pane.ctx().current_path_ /= name;
                }
                if (fs::is_directory(pane.ctx().current_path_)) {
                    pane.show_dir(list_dir(pane.ctx().current_path_, pane.ctx().show_hidden_));
                    window_.set_title(pane.ctx().current_path_.string() + " - fmgr");
                }
            }
        });
    }

    int run()
    {
        return gtk_app_->run(window_);
    }

    std::vector<fs::path> list_dir(const fs::path &dir_path, bool show_hidden)
    {
        std::vector<fs::path> paths;
        auto it = fs::directory_iterator(dir_path);
        std::copy_if(begin(it), end(it), std::back_inserter(paths), [&](const fs::path &p) {
            return show_hidden || (!p.empty() && p.filename().string()[0] != '.');
        });
        std::sort(begin(paths), end(paths), [](const fs::path &p1, const fs::path &p2) {
            if (fs::is_directory(p1) && !fs::is_directory(p2)) {
                return true;
            }
            if (!fs::is_directory(p1) && fs::is_directory(p2)) {
                return false;
            }
            return p1.compare(p2) <= 0;
        });
        return paths;
    }
};

int main(int argc, char *argv[])
{
    fmgr_app fmgr{Gtk::Application::create(argc, argv, "eu.muscar.fmgr")};
    fmgr.build_ui();
    return fmgr.run();
}

