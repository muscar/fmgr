CXX=g++
CXXFLAGS=-std=c++1z -Wall
EXTRAFLAGS=`pkg-config gtkmm-3.0 --cflags --libs` -lstdc++fs
TARGET=fmgr

all:
	$(CXX) $(CXXFLAGS) src/main.cxx -o bin/$(TARGET) $(EXTRAFLAGS)

debug:
	$(CXX) $(CXXFLAGS) -g src/main.cxx -o bin/$(TARGET).debug $(EXTRAFLAGS)

clean:
	rm -f bin/$(TARGET)*
